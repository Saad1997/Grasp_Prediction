from skimage import io
from matplotlib import pyplot as plt
import numpy as np
import cv2
import os
import glob
import argparse
from skimage.restoration import denoise_nl_means, estimate_sigma
from skimage import img_as_ubyte, img_as_float

def parse_args():
    parser = argparse.ArgumentParser(description='Image Segmentation using Histogram')

    # Network
    parser.add_argument('--original-images-path', type=str, default='/home/saad/Desktop/Masters/Master Project/literature_review/Real_Dataset/stn*_pkg***_0_****_***_im.png',help='Path to images to segment using Histogram')
    parser.add_argument('--save-path', type=str, default="/home/saad/Desktop/Masters/Master Project/small_data/segmented_data",help='Path to Dir. to save results')
    parser.add_argument('--background-image-path', type=str, default="/home/saad/Downloads/Background_cropped.png", help='Path to Background Image')
    args = parser.parse_args()
    return args

args = parse_args()
grasps=[]
#rgb_actual_save_path="/home/saad/Desktop/Masters/Master Project/small_data/segmented_data"
rgb_actual_save_path=args.save_path
mask_files_path= args.original_images_path #path to mask files

#mask_files_path= "/home/saad/Desktop/Masters/Master Project/literature_review/Real_Dataset/stn*_pkg***_0_****_***_im.png" #path to mask files
mask_files = glob.glob(mask_files_path)
print(mask_files)
for files in mask_files:
    img_ = cv2.imread(files)
    gray_image = cv2.cvtColor(img_, cv2.COLOR_BGR2GRAY)
    img=(gray_image/2).astype(np.uint8)
    hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    instances_ids = (np.nonzero(hist))[0]

    image_number=0
    j=0

    for instance in instances_ids:
        if (11 <= instance <= 19):
            segm1 = (img == instance)
            all_segments = np.zeros((img_.shape[0], img_.shape[1],3))

            all_segments[segm1]=255
            image = np.uint8(all_segments)
            #background_img = cv2.imread("/home/saad/Downloads/Background_cropped.png")
            background_img = cv2.imread(args.background_image_path)


            real_image = cv2.imread(files.replace("im", "rgb"))
            #print(files.replace("im", "rgb"))
            new_img = cv2.bitwise_and(image, real_image)

            background = (new_img == 0) #all background pixels in new images
            new_img[background[:, :, 0]] = [55, 45, 50] #set background
            name__ = os.path.split(files)[1].split(".")
            file_name = name__[0]
            cv2.imwrite(os.path.join(rgb_actual_save_path, file_name + "{}.png".format(image_number)), new_img)

            file = open(os.path.join(rgb_actual_save_path, file_name + str(image_number) + ".txt"), "a")
            # read grasps from annotated dataset
            with open(files.replace("im.png", "rgb.txt"), "r") as f:
                while True:
                    raw_info = f.readline()
                    if not raw_info:
                        j += 1
                        break
                    points = raw_info.split()

                    xc = float(points[1])
                    yc = float(points[2])
                    w__ = float(points[3])
                    h__ = float(points[4])
                    angle = float(points[5])
                    #print({"xc":xc,"yc":yc})
                    #print(all_segments[int(yc)][int(xc)][0])
                    if(all_segments[int(yc)][int(xc)][0]==255):
                        grasps = str(1) + " " + str(xc) + " " + str(yc) + " " + str(w__) + " " + str(h__) + " " + str(
                            angle) + "\n"
                        file.write(grasps)
            image_number+=1

#print(all_segments[1274][1114])
plt.imshow(all_segments)
plt.show()
