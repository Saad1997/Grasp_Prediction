
from Data_Processing import DataProcessing
import glob
import os
import math
from PIL import Image, ImageDraw
from math import degrees, radians, sin, cos, atan, sqrt
import numpy as np
import numpy.matlib as npm
import cv2
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Image Segmentation using Histogram')

    # Network
    parser.add_argument('--original-images-path', type=str, default="/home/saad/Desktop/Masters/Master Project/small_data/TestData")
    parser.add_argument('--original-images-with-converted-annotations-save-path', type=str, default="/home/saad/Desktop/Masters/Master Project/small_data/actual/", help='Save Path to Converted annotations for Actual Image')
    parser.add_argument('--resized-images-with-converted-annotations-save-path', type=str, default="/home/saad/Desktop/Masters/Master Project/small_data/resized_Data/", help='Save Path to Converted annotations for Resized Images')
    parser.add_argument('--grasp-drawn-save-path', type=str, default="/home/saad/Desktop/Masters/Master Project/small_data/grasps_drwan/", help='Save Path to Output images for True Grasp Visualizations')
    
    args = parser.parse_args()
    return args

args = parse_args()
resize_grasp=[]
resized_grasps=[]
grasp=[]
k = 0
file_path =args.original_images_path #"/home/saad/Desktop/Masters/Master Project/small_data/TestData"
rgb_actual_save_path= args.original_images_with_converted_annotations_save_path#"/home/saad/Desktop/Masters/Master Project/small_data/actual/"
rgb_actual_image_with_grasp_path= args.grasp_drawn_save_path #"/home/saad/Desktop/Masters/Master Project/small_data/grasps_drwan/"
rgb_resized_save_path=args.resized_images_with_converted_annotations_save_path#"/home/saad/Desktop/Masters/Master Project/small_data/resized_Data/"

grasp_files = glob.glob(os.path.join(file_path, 'stn*_pkg***_0_****_***_im*.txt'))
rgb_files = glob.glob(os.path.join(file_path, 'stn*_pkg***_0_****_***_im*.png'))
#print(rgb_files)
#rename_img(rgb_files, rgb_actual_save_path);##renmae actual images with actual size
j=0
for i in grasp_files:
    print(i)
    if 0 <= j <= 9:
        file_obj = open(rgb_actual_save_path+"pcd" +"000"+str(j)+"cpos.txt", "a")
        r_grasps = open(rgb_resized_save_path+"pcd" + "000" + str(j) + "cpos.txt", "a")
    elif 10 <=j <= 99:
        file_obj = open(rgb_actual_save_path+"pcd" + "00" + str(j) + "cpos.txt", "a")
        r_grasps = open(rgb_resized_save_path+"pcd" + "00" + str(j) + "cpos.txt", "a")
    else:
        file_obj = open(rgb_actual_save_path+"pcd" + "0" + str(j) + "cpos.txt", "a")
        r_grasps = open(rgb_resized_save_path+"pcd" + "0" + str(j) + "cpos.txt", "a")

    image_path=os.path.splitext(i)[0] + '.png'
    #resize_img(rgb_resized_save_path,image_path)
    grasp_image = (i.split('/')[7]).split('.')[0]
    print({"grasp_image":grasp_image})

    with open(i,"r") as f:
         while True:
                raw_info=f.readline()
                if not raw_info:
                    j+=1
                    break
                points = raw_info.split()

                xc = float(points[1])
                yc = float(points[2])
                w = float(points[3])
                h = float(points[4])
                angle= float(points[5])
                bbox = npm.repmat([[xc], [yc]], 1, 5) + \
                       np.matmul([[math.cos(angle), -math.sin(angle)],
                                  [math.sin(angle), math.cos(angle)]],
                                 [[-w / 2, w / 2, w / 2, -w / 2, w / 2 +8],
                                  [-h / 2, -h / 2, h / 2, h / 2, 0]])
                # add first point
                x1, y1 = bbox[0][0], bbox[1][0]
                # add second point
                x2, y2 = bbox[0][1], bbox[1][1]
                # add forth point
                x3, y3 = bbox[0][2], bbox[1][2]
                # add fifth point
                x4, y4 = bbox[0][3], bbox[1][3]
                #if j==0:
 #                   print(i)
 #                   print({"cos":np.cos(angle),"sin":np.sin(angle)})
 #                   print({"angle": angle, "height":h,"width":w})
                #    print({"x1":x1,"y1":y1,"x2":x2,"y2":y2,"x3":x3,"y3":y3,"x4":x4,"y4":y4})

                grasps = str(x1) + " " + str(y1) + "\n" + str(x2) + " " + str(y2) + "\n" + str(x3) + " " + str(
                    y3) + "\n" + str(x4) + " " + str(y4) + "\n"
                file_obj.write(grasps)
                print(i)
                name__ = os.path.split(i)
                file_name = name__[1]
                g = [file_name,x1, y1, x2, y2, x3, y3, x4, y4]
                grasp.append(g)

                rot_grasp=DataProcessing.resize_grasps(r_grasps,xc,yc,w,h,angle,resized_grasps)
                resize_grasp.append(rot_grasp)
    DataProcessing.rename_img(i, rgb_actual_save_path, k)
    image=DataProcessing.rename_img(i, rgb_resized_save_path,k)
    k=k+1

    #name of image with grasps drawn

    #print({"resize_grasp":resize_grasp})
    
    DataProcessing.resize_img(rgb_resized_save_path, image)
    image__ = DataProcessing.draw_grasps(resize_grasp, os.path.join(rgb_resized_save_path, image ))
    cv2.imwrite(os.path.join(rgb_actual_image_with_grasp_path,image),image__)
    resize_grasp=[]

#rgb_resized_images = glob.glob(os.path.join(rgb_resized_save_path, 'stn*_pkg***_0_****_***_rgb.png'))
#rename_img(rgb_resized_images, rgb_resized_save_path)  ##rename resized images


