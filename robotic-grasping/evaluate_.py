import argparse
import logging
import time
import os
import re
import numpy as np
import torch.utils.data
from torchvision import transforms
from hardware.device import get_device
from inference.post_process import post_process_output
from utils.data import get_dataset
from utils.dataset_processing import evaluation, grasp
from utils.visualisation.plot import save_results, visualize_grasps_
from torch.utils.data import DataLoader, Dataset
from torchvision import datasets

logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser(description='Evaluate networks')

    # Network
    parser.add_argument('--network', metavar='N', type=str, nargs='+',
                        help='Path to saved networks to evaluate')
    parser.add_argument('--input-size', type=int, default=224,
                        help='Input image size for the network')

    # Dataset
    parser.add_argument('--dataset', type=str,
                        help='Dataset Name ("cornell" or "jaquard")')
    parser.add_argument('--dataset-path', type=str,
                        help='Path to dataset')
    parser.add_argument('--use-depth', type=int, default=0,
                        help='Use Depth image for evaluation (1/0)')
    parser.add_argument('--use-rgb', type=int, default=1,
                        help='Use RGB image for evaluation (1/0)')
    parser.add_argument('--augment', action='store_true',
                        help='Whether data augmentation should be applied')
    parser.add_argument('--split', type=float, default=0,
                        help='Fraction of data for training (remainder is validation)')
    parser.add_argument('--ds-shuffle', action='store_true', default=False,
                        help='Shuffle the dataset')
    parser.add_argument('--ds-rotate', type=float, default=0.0,
                        help='Shift the start point of the dataset to use a different test/train split')
    parser.add_argument('--num-workers', type=int, default=8,
                        help='Dataset workers')

    # Evaluation
    parser.add_argument('--n-grasps', type=int, default=1,
                        help='Number of grasps to consider per image')
    parser.add_argument('--iou-threshold', type=float, default=0.30,
                        help='Threshold for IOU matching')
    parser.add_argument('--iou-eval', action='store_true',
                        help='Compute success based on IoU metric.')
    parser.add_argument('--jacquard-output', action='store_true',
                        help='Jacquard-dataset style output')

    # Misc.
    parser.add_argument('--vis', action='store_true',
                        help='Visualise the network output')
    parser.add_argument('--save', action='store_true',
                        help='save the network output')
    parser.add_argument('--cpu', dest='force_cpu', action='store_true', default=True,
                        help='Force code to run in CPU mode')
    parser.add_argument('--random-seed', type=int, default=123,
                        help='Random seed for numpy')

    args = parser.parse_args()

    if args.jacquard_output and args.dataset != 'jacquard':
        raise ValueError('--jacquard-output can only be used with the --dataset jacquard option.')
    if args.jacquard_output and args.augment:
        raise ValueError('--jacquard-output can not be used with data augmentation.')

    return args

class ImageFolderWithPaths(datasets.ImageFolder):
    def _getitem_(self,index):
        original_tuple=super(ImageFolderWithPaths,self)._getitem_(index)
        path=self.imgs[index][0]
        self.transform(path)
        tuple_with_path=(original_tuple+(path,))
        return tuple_with_path




if __name__ == '__main__':
    args = parse_args()

    # Get the compute device
    print(args.force_cpu)
    device = get_device(args.force_cpu)

    # Load Dataset
    logging.info('Loading {} Dataset...'.format(args.dataset.title()))
    dataset=ImageFolderWithPaths(args.dataset_path)
    Dataset = get_dataset(args.dataset)
    test_dataset = Dataset(args.dataset_path,
                           output_size=args.input_size,
                           ds_rotate=args.ds_rotate,
                           random_rotate=args.augment,
                           random_zoom=args.augment,
                           include_depth=0,
                           include_rgb=args.use_rgb)
    

    indices = list(range(test_dataset.length))
    split = int(np.floor(args.split * test_dataset.length))
    if args.ds_shuffle:
        np.random.seed(args.random_seed)
        np.random.shuffle(indices)
    val_indices = indices[split:]
    val_sampler = torch.utils.data.sampler.SubsetRandomSampler(val_indices)
    logging.info('Validation size: {}'.format(len(val_indices)))

    transform = transforms.Compose([
    # you can add other transformations in this list
    transforms.ToTensor()])
   # dataset.transform=transform
    test_data = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=1,
        num_workers=args.num_workers,
        sampler=val_sampler
    )
    print(enumerate(test_data))
    #print(os.path.basename(test_dataset[0].dataset.imgs[1][Dataset.FILE_PATH.value]))
    logging.info('Done')
#


# EXAMPLE USAGE:
# instantiate the dataset and dataloader

#    data = ImageFolderWithPaths("~/anaconda3/envs/saad_grasping/cornell_data_w15/TestCornellData/",transform=transform) # our custom datat_dataet
#    dataloader = torch.utils.data.DataLoader(data)


        
    for network in args.network:
        logging.info('\nEvaluating model {}'.format(network))

        # Load Network
        if(args.force_cpu):
            net = torch.load(network,map_location='cpu')
        else:
            net= torch.load(network)
        results = {'correct': 0, 'failed': 0, 'loss':0, 'losses':{}}

        if args.jacquard_output:
            jo_fn = network + '_jacquard_output.txt'
            with open(jo_fn, 'w') as f:
                pass

        start_time = time.time()
        ld=len(test_data)
        print(ld)
        with torch.no_grad():
            for idx,( x, y, didx,rot, zoom_factor) in enumerate(test_data,0):
                    img_id= lambda dx:[int(didxa) for didxa in re.split("",str(didx)) if didxa.isdigit()]
                    #print("test_image : {}".format(img_id(didx)[0]+img_id(didx)[0]))
                    magic = lambda img_id: int(''.join(str(i) for i in img_id))
                    num=magic(img_id(didx))
                    #print("test_image: {}".format((didx.split("[a")))
                    print("test_image: {}".format(num))

                    xc = x.to(device)
                    yc = [yy.to(device) for yy in y]
                    lossd = net.compute_loss(xc, yc)

                    loss = lossd['loss']

                    results['loss'] += loss.item() / ld
                    for ln, l in lossd['losses'].items():
                        if ln not in results['losses']:
                            results['losses'][ln] = 0
                        results['losses'][ln] += l.item() / ld

                    q_out, ang_out, w_out = post_process_output(lossd['pred']['pos'], lossd['pred']['cos'],
                                                    lossd['pred']['sin'], lossd['pred']['width'])

                    #print(test_data.samples[didx])
                    s,iou = evaluation.calculate_iou_match(q_out,
                                           ang_out,
                                           test_data.dataset.get_gtbb(didx, rot, zoom_factor),
                                           no_grasps=1,
                                           grasp_width=w_out,
                                           threshold=args.iou_threshold
                                           )
                    
                    print(s)

                    if s:
                        results['correct'] += 1
                    else:
                        results['failed'] += 1
                    if args.iou_eval:
                        logging.info('IOU Results: %d/%d = %f' % (results['correct'],
                                                                  results['correct'] + results['failed'],
                                                                  results['correct'] / (
                                                                              results['correct'] + results['failed'])))
                    
                                                                  
                    if args.save:
                        save_results(ggt=test_data.dataset.get_gtbb(didx, rot, zoom_factor),
                                     rgb_img=test_data.dataset.get_rgb(didx, rot, zoom_factor, normalise=False),
                                     grasp_q_img=q_out,
                                     grasp_angle_img=ang_out,
                                     no_grasps=args.n_grasps,
                                     grasp_width_img=w_out,
                                     image_num=num)
                                     #threshold=args.iou_threshold)
                    if args.vis: 
                        print()	
                        visualize_grasps_(ggt=test_data.dataset.get_gtbb(didx, rot, zoom_factor),
                                     rgb_img=test_data.dataset.get_rgb(didx, rot, zoom_factor, normalise=False),
                                     grasp_q_img=q_out,
                                     grasp_angle_img=ang_out,
                                     iou_threshold=args.iou_threshold,
                                     result_iou=iou,
                                     result=s,
				     image_num=num,
				     no_grasps=args.n_grasps,
				  
                                     grasp_width_img=w_out)
    avg_time = (time.time() - start_time) / len(test_data)
    logging.info('Average evaluation time per image: {}ms'.format(avg_time * 1000))

    del net
    torch.cuda.empty_cache()
