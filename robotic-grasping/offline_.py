import argparse
import logging
import glob
import matplotlib.pyplot as plt
import numpy as np
import torch.utils.data
from PIL import Image

from hardware.device import get_device
from inference.post_process import post_process_output
from utils.data.camera_data import CameraData
from utils.visualisation.plot import plot_results, save_results, visualize_grasps_

logging.basicConfig(level=logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser(description='Evaluate network')
    parser.add_argument('--network', type=str,
                        help='Path to saved network to evaluate')
    parser.add_argument('--use-depth', type=int, default=0,
                        help='Use Depth image for evaluation (1/0)')
    parser.add_argument('--use-rgb', type=int, default=1,
                        help='Use RGB image for evaluation (1/0)')
    parser.add_argument('--n-grasps', type=int, default=1,
                        help='Number of grasps to consider per image')
    parser.add_argument('--save', type=int, default=0,
                        help='Save the results')
    parser.add_argument('--cpu', dest='force_cpu', action='store_true', default=True,
                        help='Force code to run in CPU mode')
    parser.add_argument('--vis', type=int, default=0,
                        help='visualize the results')
    parser.add_argument('--path-to-input-images', type=str, default='/home/student2/anaconda3/envs/saad_grasping/cornell_data_w15/test_images/images/*',
                        help='path to input images')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    # Load image
    logging.info('Loading images...')

    images = glob.glob(args.path_to_input_images)    
    print(images)
    # Load Network
    logging.info('Loading model...')
    if(args.force_cpu):
            net = torch.load(args.network,map_location='cpu')
    else:
            net= torch.load(args.network)
    #net = torch.load(args.network)
    logging.info('Done')

    # Get the compute device
    device = get_device(args.force_cpu)

    img_data = CameraData(include_depth=args.use_depth, include_rgb=args.use_rgb)

    i=0
    for rgb_img in images:
      print(rgb_img)
      i+=1    
      pic = Image.open(rgb_img, 'r')
      rgb = np.array(pic)
      depth=None
      x, depth_img, rgb_img = img_data.get_data(rgb=rgb, depth=depth)
      with torch.no_grad():
        xc = x.to(device)
        pred = net.predict(xc)
        q_img, ang_img, width_img = post_process_output(pred['pos'], pred['cos'], pred['sin'], pred['width'])
        if args.vis:
            #save_results(
             #   rgb_img=img_data.get_rgb(rgb, False),
             #   grasp_q_img=q_img,
             #   grasp_angle_img=ang_img,
             #   no_grasps=args.n_grasps,
             #   grasp_width_img=width_img,
             #   image_num=i
            #)
            visualize_grasps_(
                
                rgb_img=img_data.get_rgb(rgb, False),
                grasp_q_img=q_img,
                grasp_angle_img=ang_img,
                no_grasps=args.n_grasps,
                grasp_width_img=width_img,
                image_num=i
            )
        if args.save:
            fig = plt.figure(figsize=(10, 10))
            plot_results(fig=fig,
                         rgb_img=img_data.get_rgb(rgb, False),
                         grasp_q_img=q_img,
                         grasp_angle_img=ang_img,
                         no_grasps=args.n_grasps,
                         grasp_width_img=width_img)
            fig.savefig('img_result'+str(i)+'.pdf')
