# Antipodal Robotic Grasping

This repository contains the Data Processing Pipeline and modified implementation of Generative Residual Convolutional Neural Network for Uni Bremen Plant Based Dataset.

You can find reference [paper](https://arxiv.org/abs/1909.04810) and [code](https://github.com/skumra/robotic-grasping) by authors of GRConvNet. 


## Requirements

- numpy
- opencv-python
- matplotlib
- scikit-image
- imageio
- torch
- torchvision
- torchsummary
- tensorboardX
- pyrealsense2
- Pillow

## Installation
- Checkout the robotic grasping package

- Create a virtual environment
```bash 
$ python3.6 -m venv --system-site-packages venv
```

- Source the virtual environment
```bash
$ source venv/bin/activate
```

- Install the requirements
```bash
$ pip install -r requirements.txt
```


#### Cornell Grasping Dataset

1. Download the and extract [Cornell Grasping Dataset](http://pr.cs.cornell.edu/grasping/rect_data/data.php). 

#### Uni Bremen Plant Dataset

1. You can find the our Multiobject dataset [here](https://www.dropbox.com/sh/5h8z9r4wddurhw9/AADSPT_0Dw5s82tY0Q1XZLPRa?dl=0).
2. To perform annotation segmentation and annotation conversion to cornell dataset format perform following steps.

```bash
$ cd Uni_Bremen_Data_Processing 
```

3. Perform image segmentation on original dataset.Background image can be any image of your choice carrying background only.

```bash
$ python3 hist_cv2.py --original-images-path <path to input images> --save-path <path_to_save_output_segmented_images> --background-image-path <path_to_background_image>
```
4. Convert Segmented Dataset's annotations to Cornell Dataset annotation's format.

```bash
$ python3 grasp_genrator.py --original-images-path <path to input images> --original-images-save-path <path to save original output images>  --resized-images-save-path <path to save output resized images>   --grasp-drawn-save-path <path to save output images with true grasps for visualization>  
 ```

## Model Training

A model can be trained using the `train.py` script.  Run `train.py --help` to see a full list of options.

```bash
$ python3 train_network.py --dataset cornell --dataset-path <Path To Dataset> --description training_cornell
```
Example:
```bash
$ python train.py --dataset cornell --dataset-path /home/student2/anaconda3/envs/saad_grasping/cornell_data_w15/Train_Data/ --val-dataset-path /home/student2/anaconda3/envs/saad_grasping/cornell_data_w15/Validation_data/ --description training_cornell --epoch 1000 

```

You can chnage angle threshold for evaluation by changing `angle_threshold` in `iou_train` function in `utils/dataset-processing/grasp.py` file.

The trained models will be saved in logs directory with time and date.
## Model Evaluation

The trained network can be evaluated using the `evaluate_.py` script.  Run `evaluate_.py --help` for a full set of options.
The results will be saved in Results directory.

```bash
$ python evaluate_.py --network <Path to Trained Network> --dataset cornell --dataset-path <Path to Dataset> --iou-eval
```
Example:

```bash
$ python my_evaluate.py --network logs/iou_avg0.50angle5/epoch_498_iou_0.82 --dataset cornell --dataset-path ../cornell_data_w15/new_test_data/ --use-depth 0 --use-rgb 1 --n-grasps 1 --iou-eval --vis

```
You can chnage angle threshold for evaluation by changing `angle_threshold` in `iou` function in `utils/dataset-processing/grasp.py` file.

## Model Prediction :


Prediction for trained network can be made using the `offline_.py` script.  Run `offline_.py --help` for a full set of options.

```bash
$ python3 offline.py --network <path to netwrok> --path-to-input-images <path to input images> --vis <1 for output visulization/0 otherwise>
```
Example:
```bash
$ python3 offline.py --network logs/iou_avg0.50angle15/epoch_347_iou_0.91 --vis 1
```

